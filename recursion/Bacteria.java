package recursion;

public class Bacteria {
	int pokolenie;
	Bacteria dete1;
	Bacteria dete2;
	
	Bacteria(int p) {
		pokolenie = p;
		printTabs();
		System.out.println("Раждам се " + pokolenie);
		this.divide();
		printTabs();
		System.out.println("Унищожавам се " + pokolenie);
	}
	
	void divide() {
		if (pokolenie == 3) {
			return;
		}
		this.dete1 = new Bacteria(this.pokolenie + 1);
		this.dete2 = new Bacteria(this.pokolenie + 1);
	}
	
	void printTabs() {
		for (int i = 0; i < pokolenie; i++) {
			System.out.print("  ");
		}
	}
	
	
}
