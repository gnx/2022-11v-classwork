package game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameMapPanel extends Panel {
	private static final long serialVersionUID = 1L;
	Karta karta;
	
	GameMapPanel(Karta k) {
		karta = k;
		GameKeyListener gkl = new GameKeyListener(this);
		GameMouseListener gml = new GameMouseListener(this);
		addKeyListener(gkl);
		addMouseListener(gml);
		this.setBackground(Color.GREEN);
	}
	
	public void paint(Graphics g) {
		File f = new File("hero.png");
		try {
			BufferedImage bi = ImageIO.read(f);
			g.drawImage(bi, karta.hero.posx * 30, karta.hero.posy * 30, 30, 30, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int sizexInPixels = karta.sizex * 30;
		int sizeyInPixels = karta.sizey * 30;
		
		for (int i = 0; i <= karta.sizex; i++) {
			g.drawLine(i * 30, 0, i * 30, sizeyInPixels);
		}
		for (int i = 0; i <= karta.sizey; i++) {
			g.drawLine(0, i * 30, sizexInPixels, i * 30);
		}
	}
}
