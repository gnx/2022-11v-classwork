package game;

public class Programa {
	public static void main(String[] args) {
		// създавам картата и героя
		Karta k = new Karta(5, 7);
		Hero h = new Hero(1, 5);
		k.hero = h;
		
		GameFrame f = new GameFrame(k);
		f.setLayout(null);
		
		f.setSize(f.getMaximumSize());
		f.setVisible(true);

//		// четене от файл
//		File file = new File("data.txt");
//		try {
//			Scanner scan = new Scanner(file);
//			int mapsizex = scan.nextInt();
//			int mapsizey = scan.nextInt();
//			int heroposx = scan.nextInt();
//			int heroposy = scan.nextInt();
//			
//			// създавам картата и героя
//			Karta k = new Karta(mapsizex, mapsizey);
//			Hero h = new Hero(heroposx, heroposy);
//			k.hero = h;
//			
//			GameFrame f = new GameFrame(k);
//			f.setLayout(null);
//			
//			f.setSize(f.getMaximumSize());
//			f.setVisible(true);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		
//		// записване във файл
//		File fileout = new File("zapis.txt");
//		try {
//			int variable = 20;
//			FileWriter fw = new FileWriter(fileout);
//			fw.write("" + variable);
//			fw.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
}
