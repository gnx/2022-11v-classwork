package game;


import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GameWindowListener implements WindowListener {
	GameFrame frame;
	
	GameWindowListener(GameFrame f) {
		frame = f;
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
		// десериализация
		try {
			FileInputStream fis = new FileInputStream("karta.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			frame.mapPanel.karta = (Karta)ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// сериализация
		try {
			FileOutputStream fos = new FileOutputStream("karta.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(frame.mapPanel.karta);
			oos.close();
			fos.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}