package enums;

import java.util.Scanner;

public class Programa {
	public static void main(String[] args) {
		Tree t = new Tree();
		t.height = 20;
		t.type = TreeType.Oak;
		
		for (int i = 0; i < TreeType.values().length; i++) {
			System.out.println(TreeType.values()[i]);
		}
		
		Scanner scan = new Scanner(System.in);
		String s = scan.next();
		boolean isValid = false;
		for (int i = 0; i < TreeType.values().length; i++) {
			String tt = TreeType.values()[i].toString();
			if (tt.equals(s)) {
				isValid = true;
				break;
			}
		}
		System.out.println(isValid);
	}
}
