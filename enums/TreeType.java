package enums;

public enum TreeType {
	Oak, 	// дъб
	Pine, 	// бор
	Spruce, // смърч
	Birch 	// бреза :)
}
