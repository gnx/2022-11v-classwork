package zoo;

public final class Gepard extends Bozainik implements Hodeshto {
	
	void printInfo() {
		System.out.println("Аз съм гепард.");
		System.out.println("Казвам се " + ime + ".");
		System.out.println("Тежа " + teglo + " килограма.");
	}

	public void hodi() {
		System.out.println("Аз съм гепард и ходя както го правим гепардите.");
	}
}
