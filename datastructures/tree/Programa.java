package datastructures.tree;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Programa {
	public static void main(String[] args) {
		Set<Integer> hash = new HashSet<>();
		Set<Integer> tree = new TreeSet<>(hash);
		tree.add(5);
		tree.add(23523545);
		tree.add(5);
		tree.add(9);
		tree.add(4);
		tree.add(8);
		tree.add(79);
		tree.add(7);
		System.out.println(tree);
	}
}
