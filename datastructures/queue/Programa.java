package datastructures.queue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Programa {
	public static void main(String[] args) {
		Queue<String> al = new LinkedList<>();
		al.add("Християн");
		al.add("Сидор");
		al.add("Станислав");
		al.add("Пламен Цанев");
		al.add("Пламен Недялков");
		al.add("Петър");
		al.add("Петко");
		al.add("Преслава");
		al.add("Симона");
		al.add("Хари");
		al.add("Радо");
		al.add("Румен");
		System.out.println(al.poll());
	}
}
